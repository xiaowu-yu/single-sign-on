package com.sso;

import com.sso.prop.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class SourceProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(SourceProductApplication.class, args);
    }
}
