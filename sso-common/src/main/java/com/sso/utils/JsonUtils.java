package com.sso.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;


/**
 * @program: springboot-security-sso
 * @description: 对jackson对象进行了封装
 * @author: 武文玉
 * @create: 2022-11-02 11:27
 **/
public class JsonUtils {
    private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * 将对象序列化转化为json字符串
     * @param obj
     * @return 返回一个json字符串
     */
    public static String toString(Object obj){
        if (obj == null){
            return null;
        }
        if (obj.getClass() == String.class){
            return (String) obj;
        }
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("json序列化失败:"+ obj,e);
            return null;
        }
    }

    /**
     * 将json对象解析为指定的类型对象
     * @param json
     * @param tClass
     * @return
     * @param <T>
     */
    public static <T> T toBean(String json,Class<T> tClass){
        try {
            return mapper.readValue(json,tClass);
        } catch (IOException e) {
            logger.error("json解析失败:"+ json,e);
            return null;
        }
    }

    /**
     * 将输入流解析为指定的类型对象
     * @param inputStream
     * @param tClass
     * @return
     * @param <T>
     */
    public static <T> T toBean(InputStream inputStream, Class<T> tClass){
        try {
            return mapper.readValue(inputStream,tClass);
        } catch (IOException e) {
            logger.error("json解析失败:"+ inputStream,e);
            return null;
        }
    }

    /**
     * 将json字符串解析为指定类型集合
     * @param json
     * @param eClass
     * @return
     * @param <E>
     */
    public static <E> List<E> toList(String json,Class<E> eClass){
        try {
            return mapper.readValue(json,mapper.getTypeFactory().constructCollectionType(List.class,eClass));
        } catch (IOException e) {
            logger.error("json解析失败:"+ json,e);
            return null;
        }
    }

    /**
     * 将json字符串解析为指定键值对类型对象
     * @param json
     * @param kClass
     * @param vClass
     * @return
     * @param <K>
     * @param <V>
     */
    public static <K,V> Map<K,V> toMap(String json,Class<K> kClass,Class<V> vClass) {
        try {
            return mapper.readValue(json,mapper.getTypeFactory().constructMapType(Map.class,kClass,vClass));
        } catch (IOException e) {
            logger.error("json解析失败:"+ json,e);
            return null;
        }
    }

    /**
     * 将json对象解析为复杂类型对象
     * @param json
     * @param type
     * @return
     * @param <T>
     */
    public static <T> T nativeRead(String json, TypeReference<T> type){
        try {
            return mapper.readValue(json,type);
        } catch (IOException e) {
            logger.error("json解析失败:"+ json,e);
            return null;
        }
    }

}
