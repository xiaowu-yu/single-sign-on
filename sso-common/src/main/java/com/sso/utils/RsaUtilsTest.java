package com.sso.utils;


import org.junit.jupiter.api.Test;

/**
 * @program: springboot-security-sso
 * @description:
 * @author: 武文玉
 * @create: 2022-11-02 14:57
 **/
public class RsaUtilsTest {
    private String publicFile = "G:\\auth_key\\rsa_key.pub";
    private String privateFile = "G:\\auth_key\\rsa_key";
    private String secret = "CaoChenLeiSecret";

    @Test
    public void generateKey() throws Exception {
        RsaUtils.generateKey(publicFile, privateFile, secret, 2048);
    }
}
