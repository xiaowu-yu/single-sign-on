package com.sso.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: springboot-security-sso
 * @description:
 * @author: 武文玉
 * @create: 2022-11-02 11:27
 **/
@Data
public class Payload<T> implements Serializable {
    private String id;
    private T userInfo;
    private Date expiration;
}
