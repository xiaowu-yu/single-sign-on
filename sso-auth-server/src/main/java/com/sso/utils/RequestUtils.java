package com.sso.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * @program: springboot-security-sso
 * @description:
 * @author: 武文玉
 * @create: 2022-11-02 11:32
 **/
public class RequestUtils {

    private static final Logger logger = LoggerFactory.getLogger(RequestUtils.class);

    /**
     * 从请求对象的输入流中获取指定类型对象
     * @param request
     * @param clazz
     * @return
     * @param <T>
     */
    public static <T> T read(HttpServletRequest request,Class<T> clazz){
        try {
            return JsonUtils.toBean(request.getInputStream(),clazz);
        } catch (IOException e) {
            logger.error("读取出错：" + clazz, e);
            return null;
        }
    }
}
