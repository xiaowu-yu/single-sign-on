package com.sso.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * @program: springboot-security-sso
 * @description:
 * @author: 武文玉
 * @create: 2022-11-02 11:29
 **/
@Data
public class SysRole implements GrantedAuthority {
    private Integer id;
    private String name;
    private String desc;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return name;
    }
}
