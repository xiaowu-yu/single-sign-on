package com.sso;

import com.sso.prop.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @program: springboot-security-sso
 * @description:
 * @author: 武文玉
 * @create: 2022-11-02 11:38
 **/
@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class SourceOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(SourceOrderApplication.class, args);
    }
}
